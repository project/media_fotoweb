<?php

namespace Drupal\media_fotoweb;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Base class for Fotoweb Image Fetcher plugins.
 */
abstract class ImageFetcherBase extends PluginBase implements ImageFetcherInterface, ContainerFactoryPluginInterface {

  /**
   * The Fotoweb client.
   *
   * @var \Drupal\media_fotoweb\FotowebClient
   */
  protected $client;

  /**
   * The HTTP response of the fetched image.
   *
   * @var \GuzzleHttp\Psr7\Response
   */
  protected $response;

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->response;
  }

}
