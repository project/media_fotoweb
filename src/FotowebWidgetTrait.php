<?php

namespace Drupal\media_fotoweb;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use kamermans\OAuth2\Token\RawToken;

/**
 * Provides common helper for building the Fotoweb widget.
 */
trait FotowebWidgetTrait {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Builds the Fotoweb Embedded Selection Widget as an <iframe>.
   *
   * @return array[]
   *   Returns the embedded widget render array.
   */
  protected function buildEmbeddedSelectionWidget() {
    $config = $this->configFactory->get('media_fotoweb.settings');
    $fotoweb_host = $config->get('server');
    $widget_url = $fotoweb_host . '/fotoweb/widgets/selection';
    $widget_height = $config->get('selection_widget_height');

    if ($config->get('auth_type') === 'oauth2') {
      if (!$this->isUserAuthenticated()) {
        $this->messenger()->addWarning($this->t('Your user has not been authenticated with Fotoware.'));
        return [
          'authenticate_button' => [
            '#type' => 'link',
            '#title' => $this->t('Authenticate with Fotoware'),
            '#attributes' => [
              'class' => [
                'button',
                'button--primary',
              ],
              'target' => '_blank',
            ],
            '#url' => Url::fromRoute('media_fotoweb.oauth2_callback'),
          ],
          'description' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => new FormattableMarkup(t('Please close and reopen the media library after successful authentication. Contact the administator in case of errors.'), []),
          ],
        ];
      }

      /** @var \Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence $tokenPersistence */
      $tokenPersistence = \Drupal::service('media_fotoweb.user_token_persistence');
      $token = new RawToken();
      $tokenPersistence->restoreToken($token);
      $widget_url .= '?' . http_build_query(['access_token' => $token->getAccessToken()]);
    }
    else {
      // Generate user login token and append it to widget url, when using SSO.
      if ($config->get('selection_widget_use_sso')) {
        if ($user_login_token = $this->loginManager->getLoginTokenFromAccount($this->currentUser)) {
          $widget_url .= '?' . http_build_query(['lt' => $user_login_token]);
        }
      }
    }

    // Create the widget using an <iframe>.
    $build = [
      'widget' => [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#attributes' => [
          'src' => $widget_url,
          'width' => '100%',
          'height' => $widget_height,
          'tabindex' => 0,
        ],
      ],
    ];

    // Revoking is only necessary for OAuth 2.0 based integrations.
    if ($config->get('auth_type') === 'oauth2') {
      $build['revoke_authenticate_button'] = [
        '#type' => 'link',
        '#title' => $this->t('Revoke authentication with Fotoware'),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
            'button--danger',
          ],
          'target' => '_blank',
        ],
        '#url' => Url::fromRoute('media_fotoweb.oauth2_revoke'),
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $entity_type_id = 'fotoweb';
    $entities = [];
    $selected = json_decode($form_state->getValue('fotoweb_selected', []));

    /** @var \Drupal\media\MediaTypeInterface $media_type */
    $media_type = $this->entityTypeManager->getStorage('media_type')
      ->load($entity_type_id);

    if (!empty($selected)) {
      foreach ($selected as $asset) {
        $mid = $this->entityTypeManager->getStorage('media')->getQuery()
          ->condition('field_fotoweb_identifier', $asset->href)
          ->range(0, 1)
          ->accessCheck()
          ->execute();
        if ($mid) {
          $media = $this->loadAndSyncMedia($mid, $asset);
          $entities[] = $media;
        }
        else {
          /** @var \Drupal\media\MediaInterface $media */
          $media = $this->entityTypeManager->getStorage('media')->create([
            'bundle' => $media_type->id(),
            'field_fotoweb_identifier' => $asset->href,
            'uid' => $this->currentUser->id(),
            'status' => TRUE,
            'original_data' => $asset,
          ]);

          $media->save();
          $entities[] = $media;
        }
      }
    }

    return $entities;
  }

  /**
   * Load and sync the existing media entity.
   *
   * @param int $mid
   *   The media id.
   * @param object $asset
   *   The Fotoweb asset.
   *
   * @return \Drupal\media\MediaInterface
   *   The selected media entity.
   */
  protected function loadAndSyncMedia($mid, object $asset) {
    $config = $this->configFactory->get('media_fotoweb.settings');
    $asset_update_type = $config->get('asset_update_type');

    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->entityTypeManager->getStorage('media')
      ->load(reset($mid));

    // Resync the asset data from Fotoweb on selection, when "reuse" update
    // type was set.
    if ($asset_update_type === 'reused') {
      $media->original_data = $asset;
      $media->save();
    }

    return $media;
  }

  /**
   * Checks whether the user has a persistent token which is not expired.
   *
   * @return bool
   *   True if the token exists and appears to be not expired.
   */
  protected function isUserAuthenticated() {
    /** @var \Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence $tokenPersistence */
    $tokenPersistence = \Drupal::service('media_fotoweb.user_token_persistence');
    if ($tokenPersistence->hasToken()) {
      $token = new RawToken();
      $tokenPersistence->restoreToken($token);
      if ($token->isExpired()) {
        // The access token is expired, so try to get a new one by using the
        // refresh token.
        /** @var \Drupal\media_fotoweb\FotowebClient $client */
        $client = \Drupal::service('media_fotoweb.client');
        $clientConfiguration = $client->getConfiguration();
        $clientConfiguration['persistenceType'] = 'user_token_persistence';
        $client->createClientFromConfiguration($clientConfiguration);
        try {
          if ($accessToken = $client->getAccessToken()) {
            return TRUE;
          }
        }
        catch (\Exception $e) {
          // Do nothing here.
        }
      }
      else {
        // Means we have a non expired token, so try to use it.
        return TRUE;
      }
      if (!empty($token->getAccessToken())) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
