<?php

namespace Drupal\media_fotoweb\Form;

use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Drupal\media_fotoweb\FotowebLoginManagerInterface;
use Drupal\media_fotoweb\FotowebWidgetTrait;
use Drupal\media_library\Ajax\UpdateSelectionCommand;
use Drupal\media_library\Form\AddFormBase;
use Drupal\media_library\MediaLibraryState;
use Drupal\media_library\MediaLibraryUiBuilder;
use Drupal\media_library\OpenerResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Creates a form to create media entities from Fotoweb.
 */
class FotowebBrowserForm extends AddFormBase {

  use FotowebWidgetTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Fotoweb login manager.
   *
   * @var \Drupal\media_fotoweb\FotowebLoginManagerInterface
   */
  protected $loginManager;

  /**
   * Constructs a AddFormBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_library\MediaLibraryUiBuilder $library_ui_builder
   *   The media library UI builder.
   * @param \Drupal\media_library\OpenerResolverInterface $opener_resolver
   *   The opener resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\media_fotoweb\FotowebLoginManagerInterface $login_manager
   *   The Fotoweb login manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MediaLibraryUiBuilder $library_ui_builder, OpenerResolverInterface $opener_resolver = NULL, ConfigFactoryInterface $config_factory, AccountInterface $current_user, FotowebLoginManagerInterface $login_manager) {
    parent::__construct($entity_type_manager, $library_ui_builder, $opener_resolver);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->loginManager = $login_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('media_library.ui_builder'),
      $container->get('media_library.opener_resolver'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('media_fotoweb.login_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_fotoweb_add_form';
  }

  /**
   * Create a scaffolded Fotoweb media entity.
   *
   * Scheduler 2.0 calls getEntity for every supporte entity type, even if
   * the scheduler option was not enabled.
   *
   * @return \Drupal\media\MediaInterface
   */
  public function getEntity() {
    return $this->entityTypeManager->getStorage('media')->create([
      'bundle' => 'fotoweb',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();

    // Either use the media library state from the form state. This is needed
    // to influence the form action within a media library modal.
    if (!empty($form_state->getStorage()['media_library_state'])) {
      /** @var \Drupal\media_library\MediaLibraryState $mediaLibraryState */
      $mediaLibraryState = $form_state->getStorage()['media_library_state'];
      $request->query->set('media_library_opener_id', $mediaLibraryState->get('media_library_opener_id'));
      $request->query->set('media_library_allowed_types', $mediaLibraryState->get('media_library_allowed_types', []));
      $request->query->set('media_library_selected_type', $mediaLibraryState->get('media_library_selected_type'));
      $request->query->set('media_library_remaining', $mediaLibraryState->get('media_library_remaining'));
      $request->query->set('media_library_opener_parameters', $mediaLibraryState->get('media_library_opener_parameters', []));

      $mediaLibraryState = MediaLibraryState::create($request->get('media_library_opener_id'), $request->get('media_library_allowed_types'), $request->get('media_library_selected_type'), $request->get('media_library_remaining'), $request->get('media_library_opener_parameters'));
      $request->query->set('hash', $mediaLibraryState->getHash());
      $form_state->set('media_library_state', $mediaLibraryState);
    }
    // Or build a new on based on the current request. This is needed on
    // AJAX submission.
    elseif ($request->get('media_library_opener_id')) {
      $mediaLibraryState = MediaLibraryState::fromRequest($request);
      $form_state->set('media_library_state', $mediaLibraryState);
    }

    $form = parent::buildForm($form, $form_state);

    // Invoke the build actions manually again, because we want to apply
    // it without any condition.
    $form['actions'] = $this->buildActions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildActions(array $form, FormStateInterface $form_state) {
    $actions['#type'] = 'actions';
    // Create a visual hidden submit button. The asset selection will happen on
    // click.
    $actions['submit'] = [
      '#value' => $this->t('Submit Fotoweb'),
      '#type' => 'submit',
      '#ajax' => [
        'callback' => [$this, 'submitSelectedFotowebAsset'],
        'url' => Url::fromRoute('media_fotoweb.fotoweb_browser_form'),
        'wrapper' => 'media-library-add-form-wrapper',
        'options' => [
          'query' => array_merge($this->getMediaLibraryState($form_state)->all(), [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE]),
        ],
      ],
      '#attributes' => [
        'class' => ['visually-hidden'],
      ],
    ];
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('media_fotoweb.settings');

    // Invoke the selection widget and required assets for interacting
    // with the widget.
    $form['selection_widget'] = $this->buildEmbeddedSelectionWidget();
    $form['fotoweb_selected'] = [
      '#type' => 'hidden',
    ];

    $form['selection_widget']['#attached']['library'][] = 'media_fotoweb/selection_widget';
    $form['selection_widget']['#attached']['drupalSettings']['media_fotoweb']['host'] = $config->get('server');

    return $form;
  }

  /**
   * Ajax handler for selected Fotoweb assets.
   *
   * The handler gets triggered when selecting a Fotoweb asset and takes care
   * of the media creation and submission via the media library.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response that carries the selected items and closes the modal.
   */
  public function submitSelectedFotowebAsset(array $form, FormStateInterface $form_state, Request $request) {
    $state = MediaLibraryState::fromRequest($request);

    // Prepare the media entities and pass the media ids to the media library
    // opener resolver.
    $selected_ids = array_map(function (MediaInterface $media) {
      return $media->id();
    }, $this->prepareEntities($form, $form_state));

    $mediaLibraryOpenerId = $state->get('media_library_opener_id');
    if ($mediaLibraryOpenerId === 'gutenberg.media_library.opener') {
      $response = $this->openerResolver->get($state)
        ->getSelectionResponse($state, $selected_ids)
        ->addCommand(new UpdateSelectionCommand($selected_ids))
        ->addCommand(new InvokeCommand('[aria-describedby="media-entity-browser-modal"] [type="button"]:first-child', 'click'));
    }
    else {
      return $this->openerResolver->get($state)
        ->getSelectionResponse($state, $selected_ids)
        ->addCommand(new CloseDialogCommand());
    }

    return $response;
  }

}
