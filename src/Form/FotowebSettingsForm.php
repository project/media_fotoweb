<?php

namespace Drupal\media_fotoweb\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\media_fotoweb\FotowebClient;
use Drupal\media_fotoweb\ImageFetcherManager;
use Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence;
use GuzzleHttp\Command\Exception\CommandException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure media_fotoweb settings for this site.
 */
class FotowebSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Fotoweb client.
   *
   * @var \Drupal\media_fotoweb\FotowebClient
   */
  protected $fotowebClient;

  /**
   * The Image Fetcher Manager.
   *
   * @var \Drupal\media_fotoweb\ImageFetcherManager
   */
  protected $imageFetcherManager;

  /**
   * The Api Token Persistence.
   *
   * @var \Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence
   */
  protected $apiTokenPersistence;

  /**
   * Constructs a FotowebSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\media_fotoweb\FotowebClient $fotoweb_client
   *   The Fotoweb client.
   * @param \Drupal\media_fotoweb\ImageFetcherManager $image_fetcher_manager
   *   The image fetcher manager.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence $api_token_persistence
   *   The api token persistence.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, EntityFieldManagerInterface $entity_field_manager, FotowebClient $fotoweb_client, ImageFetcherManager $image_fetcher_manager, ApiTokenPersistence $api_token_persistence) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->entityFieldManager = $entity_field_manager;
    $this->fotowebClient = $fotoweb_client;
    $this->imageFetcherManager = $image_fetcher_manager;
    $this->apiTokenPersistence = $api_token_persistence;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_field.manager'),
      $container->get('media_fotoweb.client'),
      $container->get('plugin.manager.media_fotoweb.image_fetcher'),
      $container->get('media_fotoweb.api_token_persistence')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_fotoweb_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_fotoweb.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('media_fotoweb.settings');

    $form['server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fotoweb server'),
      '#description' => $this->t('Use the server address, including protocol, excluding relative paths (/fotoweb) and trailing slashes. Example: https://fotoweb.mydomain.no'),
      '#default_value' => $config->get('server'),
      '#required' => TRUE,
    ];

    $form['auth_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Type'),
      '#description' => $this->t('The authentication type for the API access. The Full API authentication is deprecated by Fotoware.'),
      '#default_value' => $config->get('auth_type'),
      '#options' => [
        'oauth2' => $this->t('OAuth2'),
        'token' => $this->t('Full API Key (Deprecated)'),
      ],
      '#required' => TRUE,
    ];

    $form['api_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID (API)'),
      '#description' => $this->t('The Client ID for a Fotoweb <a href=":url" target="_blank">non-interactive application</a>.', [':url' => 'https://learn.fotoware.com/Integrations_and_APIs/Authorizing_applications_using_OAuth/02_Application_registration_using_OAuth_2.0']),
      '#default_value' => $config->get('api_client_id'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'oauth2'],
        ],
      ],
    ];
    $form['api_client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client Secret (API)'),
      '#description' => $this->t('The Client secret for a Fotoweb Web API application.'),
      '#default_value' => $config->get('api_client_secret'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'oauth2'],
        ],
      ],
    ];
    $form['selection_widget_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID (Selection Widget)'),
      '#description' => $this->t(
        'The Client ID for a Fotoweb
        <a href=":url" target="_blank">Web API Selection Widget application</a>.
        You will need to specify a callback URL: <em>:callback_url</em>',
        [
          ':url' =>
          'https://learn.fotoware.com/Integrations_and_APIs/Authorizing_applications_using_OAuth/02_Application_registration_using_OAuth_2.0',
          ':callback_url' => 'https://yoursite.com/media-fotoweb/oauth2/selection/callback',
        ]),
      '#default_value' => $config->get('selection_widget_client_id'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'oauth2'],
        ],
      ],
    ];
    $form['selection_widget_client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client Secret (Selection Widget)'),
      '#description' => $this->t('The Client secret for a Fotoweb Selection Widget application.'),
      '#default_value' => $config->get('selection_widget_client_secret'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'oauth2'],
        ],
      ],
    ];

    $form['full_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full API Key'),
      '#description' => $this->t('This module is using a Full Server-to-server API Authentication. See <a href="@documentation_url" target="_blank">the Fotoweb documentation</a> for more information.', ['@documentation_url' => 'https://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/Setting_the_API_key_in_the_Operations_Center']),
      '#default_value' => $config->get('full_api_key'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'token'],
        ],
      ],
    ];

    $form['selection_widget_use_sso'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use SSO for the asset selection widget?'),
      '#description' => $this->t('Enable this, when you want to use the <a href="@documentation_url" target="_blank">Single Sign-on</a> function to automatically authenticate your Drupal user to the Fotoweb widget. Users need to set their Fotoweb username in their Drupal user profile, to make the Single Sign-on work.', ['@documentation_url' => 'https://learn.fotoware.com/02_FotoWeb_8.0/Integrating_FotoWeb_with_third-party_systems/User_Authentication_for_Embeddable_Widgets#Single_Sign_On_(SSO)_for_Widgets']),
      '#default_value' => $config->get('selection_widget_use_sso'),
      '#states' => [
        'visible' => [
          ':input[name="auth_type"]' => ['value' => 'token'],
        ],
      ],
    ];

    $form['encryption_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Encryption secret'),
      '#description' => $this->t('Single-Sign on requires an <a href="@documentation_url" target="_blank">encryption secret</a> for authenticating the users.', ['@documentation_url' => 'https://learn.fotoware.com/02_FotoWeb_8.0/05_Configuring_sites/Finding_and%2F%2For_changing_the_encryption_secret']),
      '#default_value' => $config->get('encryption_secret'),
      '#states' => [
        'visible' => [
          ':input[name="selection_widget_use_sso"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['sso_user_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Fotoweb Username Field'),
      '#description' => $this->t('Select the field, that stores your users Fotoweb username (used in the Fotoweb system) for performing the Single Sign-on. If you find no appropriate field, create one on the <a href="@user_field_ui">user field configuration</a>.', ['@user_field_ui' => Url::fromRoute('entity.user.field_ui_fields')->toString()]),
      '#options' => $this->getUserSingleSignOnFieldsAsOptions(),
      '#default_value' => $config->get('sso_user_field'),
      '#states' => [
        'visible' => [
          ':input[name="selection_widget_use_sso"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['selection_widget_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Selection Widget Height'),
      '#description' => $this->t('Specify the height of the selection widget in pixels.'),
      '#default_value' => $config->get('selection_widget_height'),
    ];

    $form['file_storage_type'] = [
      '#type' => 'select',
      '#title' => $this->t('File storage type'),
      '#description' => $this->t('Original images from Fotoweb might be unnecessary big for your website usage. You can either store the original image or a smaller appropriate preview with your desired maximum width.'),
      '#options' => $this->imageFetcherManager->getImageFetcherOptionList(),
      '#default_value' => $config->get('file_storage_type'),
    ];

    $form['local_file_size_threshold'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Local file size threshold'),
      '#description' => $this->t('Define the minimal size for your locally stored images. The module will import the appropriate preview size from Fotoweb using the first preview, that matches the minimum threshold. Beware: The original image (and so the maximum) might be smaller than the threshold.'),
      '#default_value' => $config->get('local_file_size_threshold'),
      '#states' => [
        'visible' => [
          ':input[name="file_storage_type"]' => ['value' => 'rendition_image'],
        ],
        'required' => [
          ':input[name="file_storage_type"]' => ['value' => 'rendition_image'],
        ],
      ],
    ];

    $form['asset_update_type'] = [
      '#type' => 'select',
      '#title' => $this->t('When should the asset be updated?'),
      '#description' => $this->t('Define, when the asset metadata should imported. <br><em>On new created assets:</em> Only when the asset was used the first time the metadata will be imported. <br><em>On every asset selection:</em> Whenever an user selects an asset the metadata will be updated. This option can have implications (changed metadata) on old articles, that are using the same asset.'),
      '#options' => [
        'new' => $this->t('On new created assets'),
        'reused' => $this->t('On every asset selection'),
      ],
      '#default_value' => $config->get('asset_update_type'),
    ];

    if (!$this->fotowebClient->isApiAuthenticated()) {
      $form['api_authorize'] = [
        '#type' => 'submit',
        '#value' => $this->t('Authorize the OAuth2 application'),
        '#submit' => [[$this, 'submitApiAuthorize']],
        '#limit_validation_errors' => [],
      ];
      if (empty($config->get('api_client_id')) || empty($config->get('api_client_secret'))) {
        $form['api_authorize']['#disabled'] = TRUE;
        $form['api_authorize']['#suffix'] = $this->t('Please configure and store the Client ID (API) and Client Secret (API) first.');
      }
    }
    else {
      $form['api_revoke'] = [
        '#type' => 'submit',
        '#value' => $this->t('Revoke the OAuth2 authorization'),
        '#submit' => [[$this, 'submitRevokeApiAuthorize']],
        '#limit_validation_errors' => [],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('media_fotoweb.settings');
    $config->set('server', $form_state->getValue('server'));
    $config->set('auth_type', $form_state->getValue('auth_type'));
    $config->set('selection_widget_use_sso', $form_state->getValue('selection_widget_use_sso'));
    $config->set('sso_user_field', $form_state->getValue('sso_user_field'));
    $config->set('selection_widget_height', $form_state->getValue('selection_widget_height'));
    $config->set('file_storage_type', $form_state->getValue('file_storage_type'));
    $config->set('local_file_size_threshold', $form_state->getValue('local_file_size_threshold'));
    $config->set('asset_update_type', $form_state->getValue('asset_update_type'));

    if (!empty($form_state->getValue('api_client_id'))) {
      $config->set('api_client_id', $form_state->getValue('api_client_id'));
    }
    if (!empty($form_state->getValue('selection_widget_client_id'))) {
      $config->set('selection_widget_client_id', $form_state->getValue('selection_widget_client_id'));
    }
    if (!empty($form_state->getValue('full_api_key'))) {
      $config->set('full_api_key', $form_state->getValue('full_api_key'));
    }
    if (!empty($form_state->getValue('encryption_secret'))) {
      $config->set('encryption_secret', $form_state->getValue('encryption_secret'));
    }
    if (!empty($form_state->getValue('api_client_secret'))) {
      $config->set('api_client_secret', $form_state->getValue('api_client_secret'));
    }
    if (!empty($form_state->getValue('selection_widget_client_secret'))) {
      $config->set('selection_widget_client_secret', $form_state->getValue('selection_widget_client_secret'));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Authorizes the OAuth2 application.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitApiAuthorize(array &$form, FormStateInterface $form_state) {
    // Use different config types, to be able to retrieve overwritten config
    // like from the Key module, which is a good practice for storing
    // secrets and tokens.
    $config = $this->configFactory->get('media_fotoweb.settings');
    $editableConfig = $this->config('media_fotoweb.settings');

    // Initialize the client.
    $clientConfiguration = [
      'baseUrl' => $config->get('server'),
      'authType' => $config->get('auth_type'),
      'clientId' => $config->get('api_client_id'),
      'clientSecret' => !empty($config->get('api_client_secret')) ? $config->get('api_client_secret') : $config->get('api_client_secret'),
      'apiToken' => $config->get('full_api_key'),
      'grantType' => 'client_credentials',
      'client_config' => ['allow_redirects' => FALSE],
    ];
    try {
      $this->fotowebClient->createClientFromConfiguration($clientConfiguration);
      if ($rendition_service = $this->fotowebClient->fetchRenditionService()) {
        $editableConfig->set('rendition_service', $rendition_service);
      }
      else {
        $this->messenger()
          ->addWarning($this->t('No rendition service found. Therefore you cannot fetch original images. You might need to check your Fotoweb server configuration.'));
      }
      $this->messenger()->addStatus($this->t('Your site got successfully authenticated with Fotoware.'));
    }
    catch (RequestException $e) {
      $errorMessage = $e->getMessage();
      $this->messenger()
        ->addError($this->t('There was an networking error: @error_message', ['@error_message' => $errorMessage]));
    }
    catch (CommandException $e) {
      $response = $e->getResponse();
      if ($response && $response->getStatusCode() == 401) {
        $errorMessage = $this->t('The request has not been authorized. Please authenticate the API below.');
      }
      else {
        $errorMessage = $e->getMessage();
      }
      $this->messenger()
        ->addError($this->t('@error_message', ['@error_message' => $errorMessage]));
    }
    catch (\Exception $e) {
      $errorMessage = $e->getMessage();
      $this->messenger()
        ->addError($this->t('@error_message', ['@error_message' => $errorMessage]));
    }
  }

  /**
   * Revokes the oAuth2 token for the API access.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitRevokeApiAuthorize(array &$form, FormStateInterface $form_state) {
    $this->apiTokenPersistence->deleteToken();
    $this->messenger()->addStatus($this->t('Your site got disconnected from Fotoware.'));
  }

  /**
   * Returns the user SSO field option list.
   *
   * @return array
   *   Field option list as key/value pair.
   */
  protected function getUserSingleSignOnFieldsAsOptions() {
    // If there are existing fields to choose from, allow the user to reuse one.
    $options = [];
    foreach ($this->entityFieldManager->getFieldDefinitions('user', 'user') as $field_name => $field) {
      $allowed_type = in_array($field->getType(), $this->getAllowedSingleSignOnFieldTypes(), TRUE);
      // Provide only fields that are from an allowed field type, not a base
      // field or the name field.
      if ($allowed_type && (!$field->getFieldStorageDefinition()->isBaseField() || $field->getName() === 'name')) {
        $options[$field_name] = $field->getLabel();
      }
    }
    return $options;
  }

  /**
   * Returns the list of allowed field types for the SSO mapping.
   *
   * @return array
   *   A list of field types.
   */
  protected function getAllowedSingleSignOnFieldTypes() {
    return ['string'];
  }

}
