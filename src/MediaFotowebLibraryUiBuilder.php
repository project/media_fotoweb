<?php

namespace Drupal\media_fotoweb;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\media_library\MediaLibraryState;
use Drupal\media_library\MediaLibraryUiBuilder;
use Drupal\media_library\OpenerResolverInterface;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service decorator which builds the media library for Fotoweb.
 */
class MediaFotowebLibraryUiBuilder extends MediaLibraryUiBuilder {

  /**
   * Constructs a MediaFotowebLibraryUiBuilder instance.
   *
   * Makes $opener_resolver an optional parameter for installations that do not
   * use media_library.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\views\ViewExecutableFactory $views_executable_factory
   *   The views executable factory.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The currently active request object.
   * @param \Drupal\media_library\OpenerResolverInterface|null $opener_resolver
   *   The opener resolver.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, ViewExecutableFactory $views_executable_factory, FormBuilderInterface $form_builder, OpenerResolverInterface $opener_resolver = NULL) {
    parent::__construct($entity_type_manager, $request_stack, $views_executable_factory, $form_builder, $opener_resolver);
  }

  /**
   * Build the media library content area for Fotoweb assets.
   *
   * Fotoweb is made for selecting all assets via the Fotoweb Browser.
   * Therefore we remove the media items view and only keep the "form".
   *
   * @param \Drupal\media_library\MediaLibraryState $state
   *   The current state of the media library, derived from the current request.
   *
   * @return array
   *   The render array for the media library.
   */
  protected function buildLibraryContent(MediaLibraryState $state) {
    $build = parent::buildLibraryContent($state);

    // Remove the view listing for Fotoweb assets.
    if ($state->getSelectedTypeId() === 'fotoweb') {
      unset($build['view']);
    }

    return $build;
  }

}
