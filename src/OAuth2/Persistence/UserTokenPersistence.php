<?php

namespace Drupal\media_fotoweb\OAuth2\Persistence;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use kamermans\OAuth2\Persistence\TokenPersistenceInterface;
use kamermans\OAuth2\Token\TokenInterface;

/**
 * Defines the Token Persistence service for the User based token.
 */
class UserTokenPersistence implements TokenPersistenceInterface {

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * UserTokenPersistence constructor.
   *
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(UserDataInterface $userData, AccountProxyInterface $currentUser, TimeInterface $time) {
    $this->userData = $userData;
    $this->currentUser = $currentUser;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function saveToken(TokenInterface $token) {
    $this->userData->set('media_fotoweb', $this->currentUser->id(), 'user_token', $token->serialize());
  }

  /**
   * {@inheritdoc}
   */
  public function restoreToken(TokenInterface $token) {
    $data = $this->userData->get('media_fotoweb', $this->currentUser->id(), 'user_token');

    if (!is_array($data)) {
      return NULL;
    }

    return $token->unserialize($data);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteToken() {
    $this->userData->delete('media_fotoweb', $this->currentUser->id(), 'user_token');
  }

  /**
   * {@inheritdoc}
   */
  public function hasToken() {
    return !empty($this->userData->get('media_fotoweb', $this->currentUser->id(), 'user_token'));
  }

}
