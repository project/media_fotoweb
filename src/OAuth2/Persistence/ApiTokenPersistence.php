<?php

namespace Drupal\media_fotoweb\OAuth2\Persistence;

use Drupal\Core\State\StateInterface;
use kamermans\OAuth2\Persistence\TokenPersistenceInterface;
use kamermans\OAuth2\Token\TokenInterface;

/**
 * Defines the Token Persistence service for the API token.
 */
class ApiTokenPersistence implements TokenPersistenceInterface {

  const STATE_KEY = 'media_fotoweb.oauth2_token';

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * ApiTokenPersistence constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state store.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function saveToken(TokenInterface $token) {
    $this->state->set(self::STATE_KEY, $token->serialize());
  }

  /**
   * {@inheritdoc}
   */
  public function restoreToken(TokenInterface $token) {
    $data = $this->state->get(self::STATE_KEY);

    if (!is_array($data)) {
      return NULL;
    }

    return $token->unserialize($data);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteToken() {
    $this->state->delete(self::STATE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function hasToken() {
    return !empty($this->state->get(self::STATE_KEY));
  }

}
