<?php

namespace Drupal\media_fotoweb\Controller;

use Drupal\Core\Cache\CacheableRedirectResponse;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\media_fotoweb\FotowebClient;
use Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * AuthenticationController for authentication of user in fotoweb.
 */
class AuthenticationController extends ControllerBase {

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The client service.
   *
   * @var Drupal\media_fotoweb\FotowebClient
   */
  protected $client;

  /**
   * User token service.
   *
   * @var Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence
   */
  protected $userTokenPersistence;

  /**
   * Constructs a service instance.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store
   *   The tempstore service.
   * @param \Drupal\media_fotoweb\FotowebClient $client
   *   The client service.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence $user_token_persistence
   *   The user token persistence service.
   */
  public function __construct(PrivateTempStoreFactory $temp_store, FotowebClient $client, UserTokenPersistence $user_token_persistence) {
    $this->tempStore = $temp_store;
    $this->client = $client;
    $this->userTokenPersistence = $user_token_persistence;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('media_fotoweb.client'),
      $container->get('media_fotoweb.user_token_persistence')
    );
  }

  /**
   * Handles the oAuth2 user authentication.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void
   *   The response.
   */
  public function authenticateUser(Request $request) {
    if ($code = $request->get('code')) {
      // Load in security verifiers.
      $tempstore = $this->tempStore->get('media_fotoweb');
      $state = $tempstore->get('state');
      $codeVerifier = $tempstore->get('code_verifier');
      $response = new CacheableRedirectResponse('/');
      $response->getCacheableMetadata()
        ->setCacheMaxAge(0);

      if ($givenState = $request->get('state')) {
        if ($givenState != $state) {
          $this->messenger()->addError($this->t('The authentication state parameter does not match. Authentication unsuccessful. Please contact the administator.'));
          return $response;
        }
      }

      // Make a token request using the code.
      /** @var \Drupal\media_fotoweb\FotowebClient $client */
      $client = $this->client;
      $clientConfiguration = $client->getConfiguration();
      $clientConfiguration['grantType'] = 'authorization_code';
      $clientConfiguration['authorizationCode'] = $code;
      $clientConfiguration['codeVerifier'] = $codeVerifier;
      $clientConfiguration['redirectUri'] = $this->getRedirectUri();
      $client->createClientFromConfiguration($clientConfiguration);
      try {
        $client->getAccessToken();
        $this->messenger()->addStatus($this->t('You got successfully authenticated with Fotoware.'));
      }
      catch (\Exception $e) {
        $this->messenger()->addError($this->t('@error_message', ['@error_message' => $e->getMessage()]));
      }

      return $response;
    }
    else {
      // Redirect to the authorization endpoint to retrieve an authentication
      // code.
      $authorizationUrl = $this->getAuthorizationUrl();
      $response = new TrustedRedirectResponse($authorizationUrl);
      $response->getCacheableMetadata()
        ->setCacheMaxAge(0);
      return $response;
    }
  }

  /**
   * Handles the oAuth2 user authentication revocation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void
   *   The response.
   */
  public function revokeAuthenticateUser(Request $request) {
    $this->userTokenPersistence->deleteToken();
    $this->messenger()->addStatus($this->t('Your account got disconnected from Fotoware. Please reload the widget.'));
    return new RedirectResponse('/');
  }

  /**
   * Build the redirect callback URI.
   *
   * @return string
   *   The rediirect URI.
   */
  protected function getRedirectUri() {
    $url = Url::fromRoute('media_fotoweb.oauth2_callback', [], [
      'absolute' => TRUE,
      'path_processing' => FALSE,
    ]);
    return $url->toString(TRUE)->getGeneratedUrl();
  }

  /**
   * Generate the URL authorization URL for the authenticate_code method.
   *
   * @return string
   *   The authorization URL.
   *
   * @throws \Exception
   */
  protected function getAuthorizationUrl() {
    $config = $this->config('media_fotoweb.settings');
    $baseUrl = $config->get('server');
    $url = $baseUrl . '/fotoweb/oauth2/authorize';

    // Create security verifiers.
    $state = time();
    $codeVerifier = $this->generatePkceCodeVerifier();
    $codeChallenge = $this->generatePkceCodeChallenge($codeVerifier);

    // Store verifiers in the user temp sore to be able to verify them.
    $tempstore = $this->tempStore->get('media_fotoweb');
    $tempstore->set('state', $state);
    $tempstore->set('code_verifier', $codeVerifier);

    $queryParameters = [
      'response_type' => 'code',
      'client_id' => $config->get('selection_widget_client_id'),
      'client_secret' => $config->get('selection_widget_client_secret'),
      'redirect_uri' => $this->getRedirectUri(),
      'state' => $state,
      'code_challenge' => $codeChallenge,
      'code_challenge_method' => 'S256',
    ];
    return $url . '?' . http_build_query($queryParameters);
  }

  /**
   * Generate a PKCE code verifier.
   *
   * @return string
   *   The verifier.
   *
   * @throws \Exception
   */
  protected function generatePkceCodeVerifier() {
    $verifierBytes = random_bytes(64);
    $codeVerifier = rtrim(strtr(base64_encode($verifierBytes), '+/', '-_'), '=');
    return $codeVerifier;
  }

  /**
   * Generate a PKCE code challenge.
   *
   * @param string $codeVerifier
   *   The code verifier.
   *
   * @return string
   *   The code challenge.
   */
  protected function generatePkceCodeChallenge($codeVerifier) {
    $challengeBytes = hash('sha256', $codeVerifier, TRUE);
    $codeChallenge = rtrim(strtr(base64_encode($challengeBytes), '+/', '-_'), '=');
    return $codeChallenge;
  }

}
