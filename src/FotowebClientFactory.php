<?php

namespace Drupal\media_fotoweb;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\State;
use Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence;
use Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence;

/**
 * Factory that builds the FotowebClient.
 */
class FotowebClientFactory {

  /**
   * Creates the FotowebClient from configuration values.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence $user_token_persistence
   *   The user token persistence service.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence $api_token_persistence
   *   The api token persistence service.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   *
   * @return \Drupal\media_fotoweb\FotowebClient
   *   The Fotoweb Client.
   */
  public static function create(ConfigFactoryInterface $config_factory, UserTokenPersistence $user_token_persistence, ApiTokenPersistence $api_token_persistence, State $state) {
    $config = $config_factory->get('media_fotoweb.settings');

    $client_configuration = [
      'baseUrl' => $config->get('server'),
      'authType' => $config->get('auth_type'),
      'clientId' => $config->get('api_client_id'),
      'clientSecret' => $config->get('api_client_secret'),
      'selectionWidgetClientId' => $config->get('selection_widget_client_id'),
      'selectionWidgetClientSecret' => $config->get('selection_widget_client_secret'),
      'apiToken' => $config->get('full_api_key'),
      'client_config' => ['allow_redirects' => FALSE],
    ];

    $client = new FotowebClient($client_configuration, $user_token_persistence, $api_token_persistence, $state);

    $rendition_service = $config->get('rendition_service');
    if (!empty($rendition_service)) {
      $client->setRenditionService($rendition_service);
    }

    return $client;
  }

}
