<?php

namespace Drupal\media_fotoweb;

use Fotoweb\FotowebClient as FotowebAPIClient;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence;
use Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence;
use kamermans\OAuth2\OAuth2Handler;

/**
 * FotowebClient for configuration of client in fotoweb.
 */
class FotowebClient {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The Guzzle Service Client.
   *
   * @var \GuzzleHttp\Command\ServiceClientInterface
   */
  public $client;

  /**
   * The current configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The rendition service URL.
   *
   * @var string
   */
  protected $renditionService;

  /**
   * User token service.
   *
   * @var Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence
   */
  protected $userTokenPersistence;

  /**
   * Api Token service.
   *
   * @var Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence
   */
  protected $apiTokenPersistence;

  /**
   * The state service.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * FotowebClient constructor.
   *
   * @param array $configuration
   *   The guzzle service configuration.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence $user_token_persistence
   *   The user token persistence service.
   * @param \Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence $api_token_persistence
   *   The api token persistence service.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   */
  public function __construct(array $configuration, UserTokenPersistence $user_token_persistence, ApiTokenPersistence $api_token_persistence, State $state) {
    $this->createClientFromConfiguration($configuration);
    $this->userTokenPersistence = $user_token_persistence;
    $this->apiTokenPersistence = $api_token_persistence;
    $this->state = $state;
  }

  /**
   * Creates a FotowebClient from configuration.
   *
   * @param array $configuration
   *   A Fotoweb client configuration array.
   */
  public function createClientFromConfiguration(array $configuration) {
    if ($configuration['authType'] === 'oauth2') {
      $configuration['client_config']['auth'] = 'oauth';
      $configuration['grantType'] = $configuration['grantType'] ?? 'client_credentials';

      if ($configuration['grantType'] === 'authorization_code' || (isset($configuration['persistenceType']) && $configuration['persistenceType'] === 'user_token_persistence')) {
        $tokenPersistence = $this->userTokenPersistence;
        // For a user based authentication we assume it is for the selection
        // widget. Therefore use the selection widget credentials.
        $configuration['clientId'] = $configuration['selectionWidgetClientId'];
        $configuration['clientSecret'] = $configuration['selectionWidgetClientSecret'];
      }
      else {
        $tokenPersistence = $this->apiTokenPersistence;
      }
      $configuration['persistenceProvider'] = $tokenPersistence;
    }
    // If we don't have a server yet we can't create a client.
    if (!$configuration['baseUrl']) {
      \Drupal::messenger()->addMessage(
        $this->t("Until a server is added we can't create the configuration."),
        'warning'
      );
      return;
    }
    $this->setConfiguration($configuration);
    $this->client = new FotowebAPIClient($configuration);
  }

  /**
   * Sets the rendition service.
   *
   * @param string $rendition_service
   *   The rendition service URL.
   */
  public function setRenditionService($rendition_service) {
    $this->renditionService = $rendition_service;
  }

  /**
   * Returns the rendition service.
   *
   * @return string
   *   The rendition service URL.
   */
  public function getRenditionService() {
    return $this->renditionService;
  }

  /**
   * Sets the configuration.
   *
   * @param array $configuration
   *   The configuration.
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * Returns the configuration.
   *
   * @return array
   *   The configuration.
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * Return the oAuth access token.
   *
   * @return string|null
   *   The access token.
   */
  public function getAccessToken() {
    $token = NULL;

    if ($this->client->getAuthenticationMiddleware()) {
      $middleware = $this->client->getAuthenticationMiddleware();
      if ($middleware instanceof OAuth2Handler) {
        $token = $middleware->getAccessToken();
      }
    }

    return $token;
  }

  /**
   * Fetches the API Descriptor.
   *
   * @return mixed
   *   The API descriptor.
   */
  public function fetchApiDescriptor() {
    return $this->client->getApiDescriptor();
  }

  /**
   * Fetches the renditions ervice.
   *
   * @return mixed|null
   *   The rendition service or NULL on failur.
   */
  public function fetchRenditionService() {
    $renditionService = NULL;

    $apiDescriptor = $this->fetchApiDescriptor();
    if (!empty($apiDescriptor)) {
      $services = $apiDescriptor->offsetGet('services');

      if (!empty($services['rendition_request'])) {
        $renditionService = $services['rendition_request'];
      }
    }

    return $renditionService;
  }

  /**
   * Checks whether the API is authenticated.
   *
   * @return bool
   *   True if the API is authenticated.
   */
  public function isApiAuthenticated() {
    if ($this->state->get('media_fotoweb.oauth2_token')) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Forward methods to the Fotoweb Client.
   *
   * @param string $name
   *   Name of the method.
   * @param array $arguments
   *   Method arguments.
   *
   * @return mixed
   *   The method return value.
   */
  public function __call($name, array $arguments) {
    return call_user_func_array([$this->client, $name], $arguments);
  }

}
