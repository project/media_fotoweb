<?php

namespace Drupal\media_fotoweb\Plugin\EntityBrowser\Widget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\media_fotoweb\FotowebLoginManagerInterface;
use Drupal\media_fotoweb\FotowebWidgetTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Embeds the Fotoweb Selection Widget into Entity Browser.
 *
 * @EntityBrowserWidget(
 *   id = "fotoweb_selection",
 *   label = @Translation("Fotoweb"),
 *   description = @Translation("Fotoweb asset browser"),
 *   auto_select = FALSE
 * )
 */
class FotowebSelection extends WidgetBase implements ContainerFactoryPluginInterface {

  use FotowebWidgetTrait;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The Fotoweb login manager.
   *
   * @var \Drupal\media_fotoweb\FotowebLoginManagerInterface
   */
  protected $loginManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\media_fotoweb\FotowebLoginManagerInterface $login_manager
   *   The Fotoweb login manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entityFieldManager, AccountInterface $current_user, Token $token, FotowebLoginManagerInterface $login_manager, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
    $this->configFactory = $config_factory;
    $this->entityFieldManager = $entityFieldManager;
    $this->currentUser = $current_user;
    $this->token = $token;
    $this->loginManager = $login_manager;
    $this->logger = $logger_channel_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('token'),
      $container->get('media_fotoweb.login_manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    $config = $this->configFactory->get('media_fotoweb.settings');

    $form['selection_widget'] = $this->buildEmbeddedSelectionWidget();
    $form['fotoweb_selected'] = [
      '#type' => 'hidden',
    ];

    $form['selection_widget']['#attached']['library'] = ['media_fotoweb/selection_widget'];
    $form['selection_widget']['#attached']['drupalSettings']['media_fotoweb']['host'] = $config->get('server');

    // Visually hide submit button. The asset selection will happen on click.
    $form['actions']['submit']['#attributes']['class'][] = 'visually-hidden';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    $entities = $this->prepareEntities($form, $form_state);
    $this->selectEntities($entities, $form_state);

    return FALSE;
  }

}
