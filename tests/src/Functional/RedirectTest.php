<?php

namespace Drupal\Tests\media_fotoweb\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test that the redirect can be reached.
 *
 * @group media_fotoweb
 */
class RedirectTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $maximumMetaRefreshCount = 0;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media_fotoweb',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    /** @var \Symfony\Component\BrowserKit\AbstractBrowser $client */
    $client = $this->getSession()->getDriver()->getClient();
    $client->followMetaRefresh(FALSE);
    $client->followRedirects(FALSE);
    $user = $this->drupalCreateUser([
      'administer media_fotoweb',
    ]);
    $this->drupalLogin($user);
    $this->config('media_fotoweb.settings')->set('server', $this->baseUrl)->save();
  }

  /**
   * Test the controller redirects.
   */
  public function testRedirect() {
    // First we are visiting the page where we are supposed to get redirected.
    // This will send us to something like
    // example.com/fotoweb/oauth2/authorize?response_type=code&client_id=abab&client_secret=fefe&redirect_uri=http%3A%2F%2Fexample.com%2Fmedia-fotoweb%2Foauth2%2Fselection%2Fcallback&state=123&code_challenge=cdcd&code_challenge_method=xxx.
    $this->drupalGet('/media-fotoweb/oauth2/selection/callback');
    // This first one will be a 302 redirect to the oauth server.
    $status_code = $this->getSession()->getStatusCode();
    self::assertEquals(302, $status_code);
    $first_location_redirect = $this->getSession()->getResponseHeader('Location');
    self::assertStringContainsString('fotoweb/oauth2/authorize', $first_location_redirect);
    // And then we fill out something, and we come back to the same place in
    // fact. But it's kind of important that the last response was not cached,
    // since that will just trigger an infinite loop of being redirected to the
    // oauth window over there at that other server. The URL now will look
    // something like this, so we mimic that:
    // example.com/media-fotoweb/oauth2/selection/callback?code=ababfefef&state=123456.
    $this->drupalGet('/media-fotoweb/oauth2/selection/callback', [
      'query' => [
        'code' => 'ababfefef',
        'state' => '123456',
      ],
    ]);
    // Now I guess, yes. It should be a status code 302, that might be. But it
    // should certainly not be for the same location as before, because that,
    // well, will not make it possible to kind of go further in the process.
    self::assertEquals(302, $this->getSession()->getStatusCode());
    $other_location_redirect = $this->getSession()->getResponseHeader('Location');
    self::assertNotEquals($first_location_redirect, $other_location_redirect);
    self::assertStringNotContainsString('fotoweb/oauth2/authorize', $other_location_redirect);
  }

}
