<?php

namespace Drupal\Tests\media_fotoweb\Kernel;

use Drupal\media_fotoweb\FotowebClient;

/**
 * A client we use to mock responses in Kernel tests.
 */
class TestClient extends FotowebClient {

  /**
   * {@inheritdoc}
   */
  public function createClientFromConfiguration(array $configuration) {
    $this->client = new TestServiceClient($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getAsset(array $args) {
    return new TestAsset([
      'href' => 'https://example.com/api/asset/123456789',
      'metadata' => [
        'title' => 'Test asset',
      ],
      'renditions' => [
        [
          'href' => 'https://example.com/api/asset/123456789/rendition/123456789',
          'metadata' => [
            'title' => 'Test rendition',
          ],
        ],
      ],
    ]);
  }

}
