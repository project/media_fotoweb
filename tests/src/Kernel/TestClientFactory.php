<?php

namespace Drupal\Tests\media_fotoweb\Kernel;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\State;
use Drupal\media_fotoweb\FotowebClientFactory;
use Drupal\media_fotoweb\OAuth2\Persistence\ApiTokenPersistence;
use Drupal\media_fotoweb\OAuth2\Persistence\UserTokenPersistence;

/**
 * A factory to make sure we are not using the actual internet.
 */
class TestClientFactory extends FotowebClientFactory {

  /**
   * {@inheritdoc}
   */
  public static function create(ConfigFactoryInterface $config_factory, UserTokenPersistence $user_token_persistence, ApiTokenPersistence $api_token_persistence, State $state) {
    return new TestClient([
      'authType' => 'test',
      'baseUrl' => 'https://example.com',
    ], $user_token_persistence, $api_token_persistence, $state);
  }

}
