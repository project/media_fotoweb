<?php

namespace Drupal\Tests\media_fotoweb\Kernel;

use Fotoweb\FotowebClient;
use GuzzleHttp\Psr7\Response;

/**
 * Test client for using in the... heh. Client.
 */
class TestServiceClient extends FotowebClient {

  /**
   * {@inheritdoc}
   */
  public function getRenditionRequest(array $args) {
    return new TestAsset([
      'href' => 'https://example.com/api/asset/123456789/rendition/123456789',
      'metadata' => [
        'title' => 'Test rendition',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRendition($href, int $max_retries = 10) {
    return new Response(200, [], 'test');
  }

}
