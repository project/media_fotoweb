<?php

namespace Drupal\Tests\media_fotoweb\Kernel;

/**
 * Dummy value object we use.
 *
 * We can not extend the one from the library, since that gives us warnings on
 * PHP >= 8.1.0.
 */
class TestAsset implements \ArrayAccess {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists(mixed $offset) : bool {
    return array_key_exists($offset, $this->data);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet(mixed $offset) : mixed {
    return $this->data[$offset] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHref() {
    return $this->offsetGet('href');
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet(mixed $offset, mixed $value): void {
    // @todo Implement offsetSet() method.
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset(mixed $offset): void {
    // @todo Implement offsetUnset() method.
  }

}
