<?php

namespace Drupal\Tests\media_fotoweb\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;

/**
 * Test that things with the media plugin works.
 *
 * @group media_fotoweb
 */
class MediaPluginTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media',
    'image',
    'field',
    'file',
    'media_fotoweb',
    'media_library',
    'views',
    'user',
    'datetime',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('media');
    $this->installConfig(['media_fotoweb', 'system']);
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('user');
  }

  /**
   * Test the plugin, we can use it and instantiate it and use a method.
   */
  public function testCreateNewFile() {
    // Create a media item, and make sure it by default has the configuration
    // keys we expect.
    $media_item = Media::create([
      'bundle' => 'fotoweb',
    ]);
    $media_item->original_data = (object) [
      'thumbnail_width' => 666,
      'thumbnail_height' => 666,
    ];
    $media_item->save();

    /** @var \Drupal\media_fotoweb\Plugin\media\Source\Fotoweb $plugin */
    $plugin = $this->container->get('plugin.manager.media.source')->createInstance('fotoweb');
    self::assertInstanceOf('Drupal\media_fotoweb\Plugin\media\Source\Fotoweb', $plugin);
    $file = $plugin->createNewFile($media_item);
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('media_fotoweb.client')
      ->setFactory(TestClientFactory::class . '::create');
  }

}
