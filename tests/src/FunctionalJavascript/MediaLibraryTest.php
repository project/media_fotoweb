<?php

namespace Drupal\Tests\media_fotoweb\FunctionalJavascript;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;

/**
 * Tests the Media Fotoweb functionality with Media Library.
 *
 * @group media_fotoweb
 */
class MediaLibraryTest extends WebDriverTestBase {

  use ContentModerationTestTrait;
  use EntityReferenceFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'media',
    'media_library',
    'media_fotoweb',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with the 'administer media' permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $userAdmin;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an image article node type.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Create a media reference field on articles.
    $this->createEntityReferenceField(
      'node',
      'article',
      'field_media',
      'Media',
      'media',
      'default',
      ['target_bundles' => ['fotoweb']],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );
    // Add the media field to the form display.
    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('node', 'article', 'default');
    $form_display->setComponent('field_media', [
      'type' => 'media_library_widget',
    ])->save();

    // Create some users for our tests.
    $this->userAdmin = $this->drupalCreateUser([
      'access administration pages',
      'access content',
      'access media overview',
      'edit own article content',
      'create article content',
      'administer media',
    ]);

    $this->config('system.logging')
      ->set('error_level', ERROR_REPORTING_DISPLAY_ALL)
      ->save();
  }

  /**
   * Tests the media library for Fotoweb loads.
   */
  public function testMediaLibraryWidget() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('node/add/article');
    $assert_session->elementExists('css', '.js-media-library-open-button[name^="field_media"]')->click();
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->pageTextContains('Add or select media');
  }

}
